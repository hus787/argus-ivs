#ifndef CAMDISP_H
#define CAMDISP_H

#include <QtGui>
#include "detection.h"
//#include "ui_mainwindow.h"
#include "opencv2/opencv.hpp"

class camDisp : public QLabel
{
    Q_OBJECT
//    Q_ENUMS(Types wrongFile)
public:
    camDisp(QWidget *parent = 0);
    camDisp(int CamIndex,QWidget *parent=0);//camDisp for Camera input
    camDisp(QString Path, QWidget*parent=0);//camDisp for video input
    QString IAm();
    QString thePath();
    void forExternalEmission();
//    enum Types {Picture,Video,Camera} myType;
//    enum wrongFile{NO=0,YES=1} ImAWrongOne;
    QMap<QString,bool> detectionMap;
    bool myFocusStatus();
//    int myThreatLevel();
    int threat;
private:
    int framesSinceLastThreatUpdate;
    Detection* detection;
    QPixmap* IplImageToPixmapConverter(IplImage* frame);
    QString MyName;
    int camIndexNo;
    QString MyPath;
    void intializeDetectionMap();
    bool ImACopy;
    bool focus;
    void resizeEvent(QResizeEvent *);
    void run ();
    void mousePressEvent(QMouseEvent *ev);
private slots:
signals:
    void excitationMediumToAllDisp();
    void createNewTab(camDisp* me, QString MyTitle,QString Name);
    void cloneOfSourcePixmap(QPixmap *original);///clone might not be the right word
public slots:
    void settingThePixmap(QPixmap pic);
    void beingDisplayedToUser(bool YesOrNo);
    void recievePixmapFromSource(QPixmap *source);
//     void activateDetectionNo();
};

#endif // CAMDISP_H
