#include "alldisp.h"

AllDisp::AllDisp(QWidget *parent)
{
    rows=columns=1;NoOfCams=0;
    grid=new QGridLayout(this);
    setLayout(grid);
    NotSelectedLabel=new QLabel("No Camera/Video Selected",this);
    NotSelectedLabel->setVisible(false);
    NotSelectedLabel->setTextFormat(Qt::RichText);
    addDispToGrid();
}
AllDisp::AllDisp(camDisp *singleDisp, QWidget *parent){
    rows=columns=1;NoOfCams=0;
    grid=new QGridLayout(this);
    NotSelectedLabel=new QLabel("No Camera/Video Selected",this);
    setLayout(grid);
    CamMap["I'm a reflection of ONE of the contents of \"All\", so no need of path"]=singleDisp;
//    connect(this,SIGNAL(meFocused(bool)),singleDisp,SLOT(beingDisplayedToUser(bool)));
    NoOfCams=CamMap.size();
    addDispToGrid();
}
void AllDisp::setMyIndex(int in){
    myIndex=in;
}
AllDisp::~AllDisp()
{
    foreach(const QString &key,CamMap.keys()){
        CamMap[key]->beingDisplayedToUser(true);
    }
}
void AllDisp::resizeEvent(QResizeEvent *e){
//    addDispToGrid();
    setCamDispSize();
}
void AllDisp::addMoreCam(int n){
    if(NoOfCams==0){
        delete (camDisp*)grid->itemAt(0)->widget();
//        grid->invalidate();
//        grid->activate();
        grid->update();
    }
    while(n>=1){
        camDisp* cam=new camDisp();
        CamMap[QString::QString("dummy %1").arg(n)]=cam;
        connect(cam,SIGNAL(createNewTab(camDisp*,QString,QString)),this,SIGNAL(askViewerTabify(camDisp*,QString,QString)));
        n--;
    }
    NoOfCams=CamMap.size();
    addDispToGrid();
}
void AllDisp::addMoreCam(QList<int> n){
    int CamsToDisp=n.length();
    qDebug()<<CamsToDisp;
    while(CamsToDisp>=1){
        CamMap[QString::QString("dummy %1").arg(n[CamsToDisp-1])]=(new camDisp(n[CamsToDisp]));
        CamsToDisp--;
    }
    NoOfCams=CamMap.size();
    addDispToGrid();
}
void AllDisp::setRowColumnSize(){
    FindNoRowsColumns();
    SizeOfRowColumn.setWidth(this->size().width()/columns);
    SizeOfRowColumn.setHeight(this->size().height()/rows);
}
QSize AllDisp::RowColumnSize(){
    return SizeOfRowColumn;
}
void AllDisp::createAction(){
//    reSize=new QAction(this);
//    connect(reSize, SIGNAL())
}
void AllDisp::FindNoRowsColumns(){
    NoOfCams=CamMap.size();
    if(NoOfCams==1||NoOfCams==0){
        columns=rows=1;
        return ;
    }
    if (NoOfCams!=2){
        if(CheckPrime(NoOfCams)) NoOfCams++;
    }
//    int i[16]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53};
    int i=1,j,diff;
    j=diff=0;
    int *rs, *cs;
    while(i<=NoOfCams){
        if(!((NoOfCams%i)>0)){
            j=NoOfCams/i;
            if(i==1) diff=((i-j)>0?(i-j):(j-i));
            if(diff>=((i-j)>0?(i-j):(j-i))){
                int temp=((i-j)>0?(i-j):(j-i));
                diff=temp;
                rs=&i;
                cs=&j;
                rows=(*rs);//switch the two to fit.
                columns=(*cs);
            }
        }
        i++;
    }
    if (NoOfCams!=2){
        if(CheckPrime(NoOfCams-1)) NoOfCams--;
    }
}
void AllDisp::FindNoRowsColumns(int n, int &r, int &c){
    if(n==0){
        c=r=1;
        return ;
    }
    if (NoOfCams!=2){
        if(CheckPrime(NoOfCams)) NoOfCams++;
    }
//    int i[16]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53};
    int i=1,j,diff,a=0;
    j=diff=a=0;
    int *rows, *columns;
    while(a<=15){
        if(!((n%i)>0)){
            j=n/i;
            if(a==0) diff=((i-j)>0?(i-j):(j-i));
            if(diff>=((i-j)>0?(i-j):(j-i))){
                int temp=((i-j)>0?(i-j):(j-i));
                diff=temp;
                rows=&i;
                columns=&j;
                r=(*rows);
                c=(*columns);
            }
        }
        i++;
        }
}
bool AllDisp::CheckPrime (int n){
    int i=2;
    while(i<n){
        if(n%i==0){
            return false;
        }
        i++;
    }
    return true;
}
void AllDisp::clearCameraList(){
    foreach(const QString &key,CamMap.keys()){
        grid->removeWidget(CamMap[key]);
        delete CamMap[key];
    }
    grid->update();
    CamMap.clear();
    NoOfCams=CamMap.size();
    addDispToGrid();
}
void AllDisp::setCamDispSize(){
    setRowColumnSize();
    foreach(const QString &key, CamMap.keys()){
        CamMap[key]->resize(SizeOfRowColumn) ;
    }
}
void AllDisp::addDispToGrid(){
    if (NoOfCams==0){
        DisplayingNoCam();
        return;
    }
    NotSelectedLabel->setVisible(false);
    QList<QString> CamList=CamMap.keys();
    setCamDispSize();
    foreach(QString camera, CamList) grid->removeWidget(CamMap[camera]);
    int i=0, j=0, n=0;
    while (i<rows){
        j=0;
        while (j<columns&&n<CamList.length()){
           qDebug()<<"at add to dispgrid"<<"widget size"<<this->size()<<"Label size"<<CamMap[CamList[n]]->size()<<"pix size";//<<CamMap[CamList[n]]->pixmap()->size();
            grid->addWidget(CamMap[CamList[n]],i,j);
            j++;n++;
        }
        i++;
    }
}
void AllDisp::DisplayingNoCam(){
//    clearCameraList();
    NotSelectedLabel->setVisible(true);
}
void AllDisp::NoCamSelected(){
    clearCameraList();
}
void AllDisp::addCam(int camNo) {
    if(NoOfCams==0){
        NotSelectedLabel->setVisible(false);
//        grid->invalidate();
//        grid->activate();
        grid->update();
    }
    camDisp* cam=new camDisp(camNo);
    CamMap[QString::QString("Cam %1").arg(camNo)]=cam;
    NoOfCams=CamMap.size();
    connect(cam,SIGNAL(createNewTab(camDisp*,QString,QString)),this,SIGNAL(askViewerTabify(camDisp*,QString,QString)));
    connect(this,SIGNAL(meFocused(bool)),cam,SLOT(beingDisplayedToUser(bool)));
    connect(cam,SIGNAL(excitationMediumToAllDisp()),this,SLOT(calculateThreat()));
    addDispToGrid();
}
void AllDisp::addVideo(QString path){
    if(path=="") return;
    if(NoOfCams==0){
        NotSelectedLabel->setVisible(false);
    }
    camDisp* cam=new camDisp(path,this);
    CamMap[path]=cam;
    NoOfCams=CamMap.size();
    connect(cam,SIGNAL(createNewTab(camDisp*,QString,QString)),this,SIGNAL(askViewerTabify(camDisp*,QString,QString)));
    connect(this,SIGNAL(meFocused(bool)),cam,SLOT(beingDisplayedToUser(bool)));
    connect(cam,SIGNAL(excitationMediumToAllDisp()),this,SLOT(calculateThreat()));
    addDispToGrid();
}
void AllDisp::MyIndexCheck(int n){
    qDebug()<<"at MyIndexCheck "<<n;
    if(n==myIndex) emit meFocused(true);
    else emit meFocused(false);
}
void AllDisp::removeVideo(QString address){
//    qDebug()<<address;
    if(CamMap.contains(address)){
        delete CamMap[address];
        CamMap.erase(CamMap.find(address));
        NoOfCams=CamMap.size();
        calculateThreat();
        addDispToGrid();
    }
}
void AllDisp::recordCam(bool yeah, int CamId){//needs definition
}
void AllDisp::removeCam(int no){
    if(CamMap.contains(QString::QString("Cam %1").arg(no))){
        camDisp* temp;
        temp=CamMap[QString::QString("Cam %1").arg(no)];
        CamMap.remove(QString::QString("Cam %1").arg(no));
        delete temp;
        NoOfCams=CamMap.size();
        calculateThreat();
        addDispToGrid();
    }
}
QMap<QString,camDisp*> AllDisp::myCamMap(){
    return CamMap;
}
void AllDisp::calculateThreat(){
//    int numberCamsInActive=0;
//    qDebug()<<"at alldisp updatethreatbar-recieved";
    if (CamMap.size()==0){
        emit toUpdateThreatBar("All",0);
        return;
    }
    QString camDispThatIsinFocus="";
    bool AND=true;
    QList<QString> camList=CamMap.keys();
    int n=0;
    while(n<=CamMap.size()-1){
        if(CamMap.size()==1){
            camDispThatIsinFocus=camList[0];
            break;
        }
        if (CamMap[camList[n]]->myFocusStatus()==true and n==0){
            if(CamMap[camList[n+1]]->myFocusStatus()==false){
                camDispThatIsinFocus=camList[n];
                break;
            }
        }
        if (CamMap[camList[n]]->myFocusStatus()==true and AND==false){
            camDispThatIsinFocus=camList[n];
            break;
        }
        if (CamMap[camList[n]]->myFocusStatus()==true and AND==true && n>0) break;
        AND=AND and CamMap[camList[n]]->myFocusStatus();
        n++;
    }
//    qDebug()<<camDispThatIsinFocus;
    if(camDispThatIsinFocus!=""){
        int Threat=CamMap[camDispThatIsinFocus]->threat;
        emit toUpdateThreatBar(camDispThatIsinFocus,Threat);
    }
    else{
        int aggregateThreat=0;
        foreach(QString path, camList){
            aggregateThreat+=CamMap[path]->threat;
        }
        emit toUpdateThreatBar("All",aggregateThreat/CamMap.size());//sending the average threat level
    }
}
