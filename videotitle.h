#ifndef VIDEOTITLE_H
#define VIDEOTITLE_H

#include <QtGui>
#include "ui_mainwindow.h"
class videoTitle : public QLabel
{
    Q_OBJECT
private:
    QString referringPath;//the path which the label actually stand's for
    int LabelCameraIndex;
public:
    videoTitle(QString path,QList<QString> &r,Ui_MainWindow* parent);
    videoTitle(int setCameraNo,QList<QString> &r,Ui_MainWindow  * parent);
    int CameraNo();
    QList<QString> *labelList;
    QString ReferringPath();
    QCheckBox* recordButton;
signals:
    void exciteAlgoCheckBoxes(int no);
    void VideoLabelClosed(QString);
    void CameraLabelClosed(int);
    void inFormCamDisp(bool recordState);
protected:
    void mousePressEvent(QMouseEvent *event);
public slots:
    void on_recordButton_clicked(bool state);

};

#endif // VIDEOTITLE_H
