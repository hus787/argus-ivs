#include "selectalgo.h"
//#include "ui_selectalgo.h"
#include "QtGui"
selectAlgo::selectAlgo(QMap<QString, camDisp *> camList, QString algoName, QWidget *parent) :
    QDialog(parent,Qt::Tool),
    ui(new Ui::selectAlgo)
{
    ui->setupUi(this);
    theAlgoImWorkingFor=algoName;
    CamList=camList;
    QList<QString>BoxList=CamList.keys();
    foreach(QString cam, BoxList){
        QCheckBox* box=new QCheckBox((QFileInfo::QFileInfo(cam).completeBaseName()),this);
        box->setTristate(false);
        if(CamList[cam]->detectionMap[algoName]==true)box->setCheckState(Qt::Checked);
        ui->gridLayout->addWidget(box);
        MapOfBoxes[cam]=box;
    }
}

selectAlgo::~selectAlgo()
{
    delete ui;
}

void selectAlgo::on_buttonBox_accepted()
{
    bool OR=false;
    bool AND=true;
    QList<QString>paths=CamList.keys();
    foreach(QString path, paths){
        qDebug()<<path<<MapOfBoxes[path]->isChecked();
        CamList[path]->detectionMap[theAlgoImWorkingFor]=MapOfBoxes[path]->isChecked();
        OR=OR or MapOfBoxes[path]->checkState();
        AND=AND and MapOfBoxes[path]->checkState();
    }
    if((OR==false)and(AND==false)) emit algoCheckBoxStateUpdate(0);
    if((OR==true)and(AND==false)) emit algoCheckBoxStateUpdate(1);
    if((OR==true)and(AND==true)) emit algoCheckBoxStateUpdate(2);
}

int selectAlgo::updateCheckBoxStatus(){
    bool OR=false;
    bool AND=true;
    QList<QString>paths=CamList.keys();
    foreach(QString path, paths){
        qDebug()<<theAlgoImWorkingFor<<path<<MapOfBoxes[path]->isChecked();
        CamList[path]->detectionMap[theAlgoImWorkingFor]=MapOfBoxes[path]->isChecked();
        OR=OR or MapOfBoxes[path]->checkState();
        AND=AND and MapOfBoxes[path]->checkState();
    }
    if((OR==false)and(AND==false)) return 0;
    if((OR==true)and(AND==false)) return 1;
    return 2;

}


void selectAlgo::on_buttonBox_rejected()
{
    delete this;
}
