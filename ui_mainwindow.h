/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Thu Oct 27 04:03:52 2011
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QProgressBar>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QToolBox>
#include <QtGui/QToolButton>
#include <QtGui/QWidget>
#include <viewer.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionAlert_Level;
    QAction *actionAbout_Argus_0_0_1a;
    QAction *actionHelp;
    QWidget *centralWidget;
    QGridLayout *gridLayout_6;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout_5;
    QGroupBox *op_panel;
    QGridLayout *gridLayout_2;
    QToolBox *toolBox;
    QWidget *page;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_4;
    QLabel *label_6;
    QLabel *label_11;
    QLabel *algo_1;
    QCheckBox *algo_1all;
    QToolButton *algo_1sel;
    QLabel *algo_2;
    QCheckBox *algo_2all;
    QToolButton *algo_2sel;
    QLabel *algo_3;
    QCheckBox *algo_3all;
    QToolButton *algo_3sel;
    QWidget *page_2;
    QGridLayout *gridLayout_3;
    QToolButton *toolButton;
    QWidget *page_3;
    QGridLayout *gridLayout_7;
    QToolButton *toolButton_2;
    QWidget *page_4;
    QGridLayout *gridLayout_8;
    QGridLayout *gridLayout_4;
    QLabel *threatLabel;
    QProgressBar *threatLevel;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer;
    QGridLayout *gridLayout_11;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label;
    viewer *Viewer;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuView;
    QMenu *menuTools;
    QMenu *menuWindow;
    QMenu *menuHelp;
    QStatusBar *statusBar;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(756, 492);
        MainWindow->setAcceptDrops(true);
        MainWindow->setAutoFillBackground(false);
        MainWindow->setTabShape(QTabWidget::Triangular);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionAlert_Level = new QAction(MainWindow);
        actionAlert_Level->setObjectName(QString::fromUtf8("actionAlert_Level"));
        actionAbout_Argus_0_0_1a = new QAction(MainWindow);
        actionAbout_Argus_0_0_1a->setObjectName(QString::fromUtf8("actionAbout_Argus_0_0_1a"));
        actionHelp = new QAction(MainWindow);
        actionHelp->setObjectName(QString::fromUtf8("actionHelp"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_6 = new QGridLayout(centralWidget);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        op_panel = new QGroupBox(centralWidget);
        op_panel->setObjectName(QString::fromUtf8("op_panel"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(op_panel->sizePolicy().hasHeightForWidth());
        op_panel->setSizePolicy(sizePolicy);
        op_panel->setMinimumSize(QSize(195, 0));
        gridLayout_2 = new QGridLayout(op_panel);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        toolBox = new QToolBox(op_panel);
        toolBox->setObjectName(QString::fromUtf8("toolBox"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        page->setGeometry(QRect(0, 0, 175, 223));
        layoutWidget = new QWidget(page);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(8, 0, 160, 93));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 0, 0, 1, 1);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 0, 1, 1, 1);

        label_11 = new QLabel(layoutWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 0, 2, 1, 1);

        algo_1 = new QLabel(layoutWidget);
        algo_1->setObjectName(QString::fromUtf8("algo_1"));

        gridLayout->addWidget(algo_1, 1, 0, 1, 1);

        algo_1all = new QCheckBox(layoutWidget);
        algo_1all->setObjectName(QString::fromUtf8("algo_1all"));
        algo_1all->setTristate(false);

        gridLayout->addWidget(algo_1all, 1, 1, 1, 1);

        algo_1sel = new QToolButton(layoutWidget);
        algo_1sel->setObjectName(QString::fromUtf8("algo_1sel"));

        gridLayout->addWidget(algo_1sel, 1, 2, 1, 1);

        algo_2 = new QLabel(layoutWidget);
        algo_2->setObjectName(QString::fromUtf8("algo_2"));

        gridLayout->addWidget(algo_2, 2, 0, 1, 1);

        algo_2all = new QCheckBox(layoutWidget);
        algo_2all->setObjectName(QString::fromUtf8("algo_2all"));
        algo_2all->setTristate(false);

        gridLayout->addWidget(algo_2all, 2, 1, 1, 1);

        algo_2sel = new QToolButton(layoutWidget);
        algo_2sel->setObjectName(QString::fromUtf8("algo_2sel"));

        gridLayout->addWidget(algo_2sel, 2, 2, 1, 1);

        algo_3 = new QLabel(layoutWidget);
        algo_3->setObjectName(QString::fromUtf8("algo_3"));

        gridLayout->addWidget(algo_3, 3, 0, 1, 1);

        algo_3all = new QCheckBox(layoutWidget);
        algo_3all->setObjectName(QString::fromUtf8("algo_3all"));
        algo_3all->setTristate(false);

        gridLayout->addWidget(algo_3all, 3, 1, 1, 1);

        algo_3sel = new QToolButton(layoutWidget);
        algo_3sel->setObjectName(QString::fromUtf8("algo_3sel"));

        gridLayout->addWidget(algo_3sel, 3, 2, 1, 1);

        toolBox->addItem(page, QString::fromUtf8("Select Algorithm"));
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        page_2->setGeometry(QRect(0, 0, 175, 228));
        gridLayout_3 = new QGridLayout(page_2);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        toolButton = new QToolButton(page_2);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setLayoutDirection(Qt::LeftToRight);

        gridLayout_3->addWidget(toolButton, 0, 0, 1, 1);

        toolBox->addItem(page_2, QString::fromUtf8("Select Camera"));
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        page_3->setGeometry(QRect(0, 0, 175, 228));
        gridLayout_7 = new QGridLayout(page_3);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        toolButton_2 = new QToolButton(page_3);
        toolButton_2->setObjectName(QString::fromUtf8("toolButton_2"));

        gridLayout_7->addWidget(toolButton_2, 0, 0, 1, 1);

        toolBox->addItem(page_3, QString::fromUtf8("Select Video For Surveilence"));
        page_4 = new QWidget();
        page_4->setObjectName(QString::fromUtf8("page_4"));
        page_4->setGeometry(QRect(0, 0, 175, 228));
        gridLayout_8 = new QGridLayout(page_4);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        toolBox->addItem(page_4, QString::fromUtf8("Record"));

        gridLayout_2->addWidget(toolBox, 0, 0, 1, 1);


        gridLayout_5->addWidget(op_panel, 0, 0, 1, 1);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        threatLabel = new QLabel(centralWidget);
        threatLabel->setObjectName(QString::fromUtf8("threatLabel"));
        sizePolicy.setHeightForWidth(threatLabel->sizePolicy().hasHeightForWidth());
        threatLabel->setSizePolicy(sizePolicy);
        threatLabel->setMinimumSize(QSize(84, 0));
        threatLabel->setLayoutDirection(Qt::LeftToRight);
        threatLabel->setWordWrap(true);

        gridLayout_4->addWidget(threatLabel, 0, 0, 1, 1);

        threatLevel = new QProgressBar(centralWidget);
        threatLevel->setObjectName(QString::fromUtf8("threatLevel"));
        sizePolicy.setHeightForWidth(threatLevel->sizePolicy().hasHeightForWidth());
        threatLevel->setSizePolicy(sizePolicy);
        threatLevel->setMinimumSize(QSize(100, 0));
        threatLevel->setValue(0);
        threatLevel->setTextDirection(QProgressBar::TopToBottom);

        gridLayout_4->addWidget(threatLevel, 0, 1, 1, 1);


        gridLayout_5->addLayout(gridLayout_4, 1, 0, 1, 1);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_5->addWidget(label_2, 2, 0, 1, 1);


        horizontalLayout->addLayout(gridLayout_5);

        horizontalSpacer = new QSpacerItem(13, 376, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        gridLayout_11 = new QGridLayout();
        gridLayout_11->setSpacing(6);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        horizontalSpacer_2 = new QSpacerItem(388, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_11->addItem(horizontalSpacer_2, 1, 0, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);

        gridLayout_11->addWidget(label, 1, 1, 1, 1);

        Viewer = new viewer(centralWidget);
        Viewer->setObjectName(QString::fromUtf8("Viewer"));
        Viewer->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(Viewer->sizePolicy().hasHeightForWidth());
        Viewer->setSizePolicy(sizePolicy2);
        Viewer->setMinimumSize(QSize(511, 351));
        Viewer->setAutoFillBackground(false);
        Viewer->setTabShape(QTabWidget::Triangular);
        Viewer->setTabsClosable(true);
        Viewer->setMovable(false);

        gridLayout_11->addWidget(Viewer, 0, 0, 1, 2);


        horizontalLayout->addLayout(gridLayout_11);


        gridLayout_6->addLayout(horizontalLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 756, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        menuWindow = new QMenu(menuBar);
        menuWindow->setObjectName(QString::fromUtf8("menuWindow"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuHelp->setEnabled(true);
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        mainToolBar->setEnabled(false);
        MainWindow->addToolBar(Qt::BottomToolBarArea, mainToolBar);
#ifndef QT_NO_SHORTCUT
        algo_1->setBuddy(algo_1all);
        algo_2->setBuddy(algo_2all);
        algo_3->setBuddy(algo_3all);
#endif // QT_NO_SHORTCUT

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuWindow->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionOpen);
        menuTools->addAction(actionAlert_Level);
        menuHelp->addAction(actionHelp);
        menuHelp->addSeparator();
        menuHelp->addAction(actionAbout_Argus_0_0_1a);

        retranslateUi(MainWindow);

        toolBox->setCurrentIndex(0);
        Viewer->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Argus-Security Solutions", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("MainWindow", "Open", 0, QApplication::UnicodeUTF8));
        actionAlert_Level->setText(QApplication::translate("MainWindow", "Alert Level", 0, QApplication::UnicodeUTF8));
        actionAbout_Argus_0_0_1a->setText(QApplication::translate("MainWindow", "About Argus 0.0.1a", 0, QApplication::UnicodeUTF8));
        actionHelp->setText(QApplication::translate("MainWindow", "Help", 0, QApplication::UnicodeUTF8));
        op_panel->setTitle(QApplication::translate("MainWindow", "Operation Panel", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "Apply * to:", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MainWindow", "All", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("MainWindow", "Select Camera", 0, QApplication::UnicodeUTF8));
        algo_1->setText(QApplication::translate("MainWindow", "*Algo 1", 0, QApplication::UnicodeUTF8));
        algo_1all->setText(QString());
        algo_1sel->setText(QApplication::translate("MainWindow", "Select", 0, QApplication::UnicodeUTF8));
        algo_2->setText(QApplication::translate("MainWindow", "*Algo 2", 0, QApplication::UnicodeUTF8));
        algo_2all->setText(QString());
        algo_2sel->setText(QApplication::translate("MainWindow", "Select", 0, QApplication::UnicodeUTF8));
        algo_3->setText(QApplication::translate("MainWindow", "*Algo 3", 0, QApplication::UnicodeUTF8));
        algo_3all->setText(QString());
        algo_3sel->setText(QApplication::translate("MainWindow", "Select", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(page), QApplication::translate("MainWindow", "Select Algorithm", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("MainWindow", "Select Camera(s)  ", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(page_2), QApplication::translate("MainWindow", "Select Camera", 0, QApplication::UnicodeUTF8));
        toolButton_2->setText(QApplication::translate("MainWindow", "Select Video", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(page_3), QApplication::translate("MainWindow", "Select Video For Surveilence", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(page_4), QApplication::translate("MainWindow", "Record", 0, QApplication::UnicodeUTF8));
        threatLabel->setText(QApplication::translate("MainWindow", "Threat Level at:", 0, QApplication::UnicodeUTF8));
        threatLevel->setFormat(QApplication::translate("MainWindow", "%p%", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Date/time", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Argus-Security Solutions", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
        menuView->setTitle(QApplication::translate("MainWindow", "View", 0, QApplication::UnicodeUTF8));
        menuTools->setTitle(QApplication::translate("MainWindow", "Tools", 0, QApplication::UnicodeUTF8));
        menuWindow->setTitle(QApplication::translate("MainWindow", "Window", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("MainWindow", "Help", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
