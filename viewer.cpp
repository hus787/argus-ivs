#include "viewer.h"

viewer::viewer(QWidget *parent)
{
    TotalTabs=tabMap.size()+1;
    AllDisp*all=new AllDisp();
    all->setMyIndex(0);
    connect(all,SIGNAL(askViewerTabify(camDisp*,QString,QString)),this,SLOT(UniCamDisp(camDisp*,QString,QString)));
    tabMap[QString::QString("All")]=all;
    addTab(tabMap[QString::QString("All")],"All");
    tabNames<<QString::QString("All");
    connect(this, SIGNAL(tabCloseRequested(int)),this ,SLOT(closeTab(int)));
    connect(this, SIGNAL(currentChanged(int)),((AllDisp*)widget(0)),SLOT(MyIndexCheck(int)));
}
void viewer::closeTab(int index){
    if(index!=0){
        qDebug()<<tabMap.remove(tabMap.key(((AllDisp*)widget(index))));
        foreach(const QString &key, tabMap.keys()){
            tabMap[key]->setMyIndex(indexOf(tabMap[key]));
        }
        delete ((AllDisp*)widget(index));
        setCurrentWidget((AllDisp*)widget(0));
        TotalTabs--;
    }
}
void viewer::closeTab1(QString tabName){
//    qDebug()<<"at closeTab1"<<tabName;
    int ind=indexOf(tabMap[tabName]);
//    qDebug()<<"index that will be closed"<<ind;
    tabMap.remove(tabName);
    foreach(const QString &key, tabMap.keys()){
        tabMap[key]->setMyIndex(indexOf(tabMap[key]));
    }
    setCurrentWidget((AllDisp*)widget(0));
    delete ((AllDisp*)widget(ind));
    TotalTabs--;
}

void viewer::UniCamDisp(camDisp *cam, QString TabTitle,QString NameOfTab){
    if(tabMap.contains(TabTitle)){
//        qDebug()<<"already opened";
//        qDebug()<<tabNames.indexOf();
        setCurrentIndex(indexOf(tabMap[TabTitle]));
        return;
    }
    TotalTabs++;
    camDisp* camDuplicate=new camDisp();
    connect(cam, SIGNAL(cloneOfSourcePixmap(QPixmap*)),camDuplicate,SLOT(recievePixmapFromSource(QPixmap*)));
    emit cam->forExternalEmission();
    AllDisp* Cam=new AllDisp(camDuplicate,this);
    if(TabTitle==""){
        addTab(Cam,NameOfTab);
        tabMap[NameOfTab]=Cam;
    }
    else{
        addTab(Cam,(QFileInfo::QFileInfo(TabTitle).completeBaseName()));
        tabMap[TabTitle]=Cam;
    }
    connect(this, SIGNAL(currentChanged(int)),Cam,SLOT(MyIndexCheck(int)));
    connect(Cam, SIGNAL(meFocused(bool)),cam,SLOT(beingDisplayedToUser(bool)));
    Cam->setMyIndex(TotalTabs-1);
}
