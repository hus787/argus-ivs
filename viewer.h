#ifndef VIEWER_H
#define VIEWER_H

#include <QtGui>
#include"alldisp.h"
#include"camdisp.h"
class viewer : public QTabWidget
{
    Q_OBJECT
public:
    viewer(QWidget *parent = 0);
signals:
    void tabCloseCheck();
    void toThreatBar();
private:
    QList<AllDisp*> tabList;
    QList<QString>tabNames;
    QMap<QString, AllDisp*> tabMap;
    int TotalTabs;
public slots:
    void closeTab1(QString tabName);
    void closeTab(int index);
    void UniCamDisp(camDisp* cam, QString TabTitle, QString NameofTab);
};
#endif // VIEWER_H
