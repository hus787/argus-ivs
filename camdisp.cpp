#include "camdisp.h"
#include "ui_mainwindow.h"

camDisp::camDisp(QWidget *parent){
    setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding);
    ImACopy=true;
}
camDisp::camDisp(int index,QWidget *parent){
    setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
    intializeDetectionMap();
    MyName=QString::QString("Cam %1").arg(index);
    camIndexNo=index;
    MyPath="";
    ImACopy=false;
    detection=new Detection(index,this);
    focus=true;
    connect(detection, SIGNAL(PixmapToBeDisplayed(QPixmap)),this,SLOT(settingThePixmap(QPixmap)));
}
camDisp::camDisp(QString Path, QWidget *parent){
    setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    ImACopy=false;
    intializeDetectionMap();
    MyPath=Path;
    focus=true;
    detection=new Detection(Path,this);
    connect(detection, SIGNAL(PixmapToBeDisplayed(QPixmap)),this,SLOT(settingThePixmap(QPixmap)));
    detection->start();
}
void camDisp::resizeEvent(QResizeEvent *){
//    if(pixmap()!=NULL){
//        qDebug()<<"pixmap resize";
//        setPixmap(pixmap()->scaled(size(),Qt::KeepAspectRatio));
//    }
}
QString camDisp::IAm(){
    return MyName;
}
void camDisp::mousePressEvent(QMouseEvent *ev){
    if (ev->button()==Qt::LeftButton){
        if(!ImACopy)emit createNewTab(this,thePath(),IAm());
        qDebug()<<"CamDisp MouseEvent";
    }
        ;// add Tab to right
}

void camDisp::beingDisplayedToUser(bool YesOrNo){
    qDebug()<<MyPath<<"being displayed to user"<<YesOrNo;
    focus=YesOrNo;
}
void camDisp::settingThePixmap(QPixmap pic){
//    qDebug()<<"got the emission";
    setPixmap(pic.scaled(size(),Qt::KeepAspectRatio));
    if(framesSinceLastThreatUpdate>35){
        emit excitationMediumToAllDisp();
        framesSinceLastThreatUpdate=0;
    }
    emit cloneOfSourcePixmap(&pic);
    framesSinceLastThreatUpdate++;
}

QString camDisp::thePath(){
    return MyPath;
}

void camDisp::recievePixmapFromSource(QPixmap *source){
    QPixmap temp=*source;
    setPixmap(temp.scaled(size(),Qt::KeepAspectRatio));
//    qDebug()<<"got the clone";
}

void camDisp::forExternalEmission(){
    emit cloneOfSourcePixmap((QPixmap*)pixmap());
}
void camDisp::intializeDetectionMap(){
    detectionMap["algo 1"]=false;
    detectionMap["algo 2"]=false;
    detectionMap["algo 3"]=false;
    detectionMap["algo 4"]=false;
    detectionMap["algo 5"]=false;
}
bool camDisp::myFocusStatus(){
    return focus;
}

//int camDisp::myThreatLevel(){
//    return threat;
//}
//void activateDetectionNo(){
//}
