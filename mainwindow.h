#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "QtGui"
#include"ui_mainwindow.h"

namespace Ui {
    class MainWindow;

}

class MainWindow : public QMainWindow , public Ui::MainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void CheckNoOfCamsConnected();
public slots:

signals:
    void NumberOfCams(QList<int> n);
//    void tabSelected(int tabIndex);
    void sendVideoInfoToAll(QString pathOfVideo);
    void sendCameraInfoToAll(int CamIndex);
private slots:
    void addCamLabelToPage2(int ind);

    void addVideoLabelToPage3(QString Path);

    void on_algo_1sel_clicked();

    void on_checkBox_stateChanged(int m);

    void on_toolButton_2_clicked();

    void on_progressBar_valueChanged(int value);

    void on_toolButton_clicked();

    void on_algo_2sel_clicked();

    void on_algo_3sel_clicked();

    void on_algo_1all_stateChanged(int n);

    void on_algo_3all_stateChanged(int n);

    void on_algo_2all_stateChanged(int n);

    void updateThreatBar(QString path,int threat);//updates the threat bar and label with the new one's

private:
    QList<QString> labelNames;
    void createConnections();
    QList <int> IndexesOfCamsToBeDisplayed;
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
