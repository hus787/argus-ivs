#ifndef ALLDISP_H
#define ALLDISP_H

#include"QtGui"
#include"camdisp.h"

//namespace Ui {
//    class Widget;
////    class camDisp;
//}

class AllDisp : public QWidget//, public Ui_Widget
{
    Q_OBJECT
public:
    AllDisp(QWidget *parent = 0);
    AllDisp(camDisp* singleDisp,QWidget *parent=0);
    ~AllDisp();
    void FindNoRowsColumns(int n, int &r, int &c);
    bool CheckPrime (int n);
    QSize RowColumnSize();
    void setNumberOfCams(int n);
    void NoCamSelected();
    void setMyIndex(int in);
    QMap<QString,camDisp*> myCamMap();
private:
    int myIndex;
    void clearCameraList();
    void DisplayingNoCam();
    QGridLayout* grid;
    void setCamDispSize();
    QMap<QString, camDisp*> CamMap;//path/index no. of video/cam
    void addDispToGrid();
    void resizeEvent(QResizeEvent *e);
    void setRowColumnSize();
    int columns,rows, NoOfCams;
    void FindNoRowsColumns();
    QSize SizeOfRowColumn;
    QLabel* NotSelectedLabel;
    bool IllBeDisplayingASingleCam;//won't be using this
    void createAction();
    QAction* reSize;
//    Ui::Widget *ui;
//    CamDisp disp;
signals:
    void toUpdateThreatBar(QString, int );
    void meFocused(bool check);
    void askViewerTabify(camDisp* TabName, QString NameOfTab,QString TheName);
//    void tabNumberClosed(int all);
public slots:
    void calculateThreat();
    void removeVideo(QString address);
    void removeCam(int no);
    void recordCam(bool yeah,int CamId);
    void MyIndexCheck(int n);
    void addMoreCam(int n);
    void addMoreCam(QList<int> n);
    void addCam(int camNo);
    void addVideo(QString path);
};

#endif // ALLDISP_H
