#ifndef SELECTALGO_H
#define SELECTALGO_H

#include <QDialog>
#include"camdisp.h"
#include"ui_selectalgo.h"

namespace Ui {
    class selectAlgo;
}

class selectAlgo : public QDialog
{
    Q_OBJECT

public:
    selectAlgo(QMap<QString,camDisp*>camList,QString algoName,QWidget *parent = 0);
    ~selectAlgo();
    int updateCheckBoxStatus();
signals:
    int algoCheckBoxStateUpdate(int state);

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::selectAlgo *ui;
    QString theAlgoImWorkingFor;
    QMap<QString,camDisp*> CamList;
    QMap<QString,QCheckBox*> MapOfBoxes;
};

#endif // SELECTALGO_H
