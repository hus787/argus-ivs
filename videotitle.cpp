#include "videotitle.h"
#include"mainwindow.h"
#include "ui_mainwindow.h"
videoTitle::videoTitle(QString path, QList<QString> &r,Ui_MainWindow* parent) :
    QLabel()
{
    labelList=&r;
    setFrameShadow(QFrame::Raised);
    setFrameShape(QFrame::Panel);
    referringPath=path;
    LabelCameraIndex=-1;
    connect(this,SIGNAL(VideoLabelClosed(QString)), parent->Viewer,SLOT(closeTab1(QString)));
}

videoTitle::videoTitle(int setCameraNo, QList<QString> &r,Ui_MainWindow* parent=0):recordButton(new QCheckBox(this)),QLabel(){
    labelList=&r;
    setFrameShadow(QFrame::Raised);
    setFrameShape(QFrame::Panel);
    referringPath="";
    LabelCameraIndex=setCameraNo;
    connect(this,SIGNAL(VideoLabelClosed(QString)), parent->Viewer,SLOT(closeTab1(QString)));
}

void videoTitle::mousePressEvent(QMouseEvent *event){
    if(event->button()==Qt::MidButton) {
        if (LabelCameraIndex==-1){
            emit VideoLabelClosed(ReferringPath());
            labelList->removeOne(ReferringPath());
        }
        else{
            labelList->removeOne(QString::QString("Cam %1").arg(CameraNo()));
            emit VideoLabelClosed(QString::QString("Cam %1").arg(CameraNo()));
            emit CameraLabelClosed(CameraNo());
            delete recordButton;
        }
        emit exciteAlgoCheckBoxes(4);
        delete this;
    }
}

QString videoTitle::ReferringPath(){
    return referringPath;
}

int videoTitle::CameraNo(){
    return LabelCameraIndex;
}

void videoTitle::on_recordButton_clicked(bool state){
//    emit
}
