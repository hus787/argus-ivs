#include "mainwindow.h"
#include "opencv2/opencv.hpp"
#include "videotitle.h"
#include"selectalgo.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    setupUi(this);
    createConnections();

//    ((AllDisp*)Viewer->widget(0))->addMoreCam(2);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_checkBox_stateChanged(int m)
{
    if(m==2){
        CheckNoOfCamsConnected();
        emit NumberOfCams(IndexesOfCamsToBeDisplayed);
        return;
        }
    if(m==0){
        ((AllDisp*)(Viewer->widget(0)))->NoCamSelected();
    }

}

void MainWindow::CheckNoOfCamsConnected(){
    IndexesOfCamsToBeDisplayed.clear();
    int n=1;
    CvCapture* test=0;
    forever{
        test=cvCaptureFromCAM(0);
//        qDebug()<<test;
        cvQueryFrame(test);
        cvReleaseCapture(&test);
        /*if(test==0)*/break;
        IndexesOfCamsToBeDisplayed<<n;
        n++;
    }
}

void MainWindow::on_toolButton_2_clicked(){
    emit sendVideoInfoToAll(QFileDialog::getOpenFileName(this,"openFile",NULL,"Videos (*.mp4 *.avi)"));
}

void MainWindow::on_progressBar_valueChanged(int value)
{
    threatLevel->setValue(value);
}
void MainWindow::createConnections(){
//    connect(this,SIGNAL(sendCameraInfoToAll(int)),(AllDisp*)(Viewer->widget(0),SLOT()))
    connect (this, SIGNAL(NumberOfCams(QList<int>)),Viewer->widget(0),SLOT(addMoreCam(QList<int>)));//not useful for now
    connect(this,SIGNAL(threatLabel(int)),this,SLOT(on_progressBar_valueChanged(int)));
    connect(this, SIGNAL(sendVideoInfoToAll(QString)),(AllDisp*)(Viewer->widget(0)),SLOT(addVideo(QString)));
    connect(this,SIGNAL(sendVideoInfoToAll(QString)),this,SLOT(addVideoLabelToPage3(QString)));
    connect(this, SIGNAL(sendCameraInfoToAll(int)),(AllDisp*)(Viewer->widget(0)),SLOT(addCam(int)));
    connect(this,SIGNAL(sendCameraInfoToAll(int)),this, SLOT(addCamLabelToPage2(int)));
    connect((AllDisp*)(Viewer->widget(0)),SIGNAL(toUpdateThreatBar(QString,int)),this,SLOT(updateThreatBar(QString,int)));
}

void MainWindow::addVideoLabelToPage3(QString Path){
    if(Path=="") return;
    if(labelNames.contains(Path)) return;
    labelNames<<Path;
    videoTitle* videoLabel=new videoTitle(Path,labelNames,this);
    connect(videoLabel,SIGNAL(exciteAlgoCheckBoxes(int)),this,SLOT(on_algo_1all_stateChanged(int)));
    connect(videoLabel,SIGNAL(exciteAlgoCheckBoxes(int)),this,SLOT(on_algo_2all_stateChanged(int)));
    connect(videoLabel,SIGNAL(exciteAlgoCheckBoxes(int)),this,SLOT(on_algo_3all_stateChanged(int)));
    on_algo_2all_stateChanged(4);
    on_algo_1all_stateChanged(4);
    on_algo_3all_stateChanged(4);
    videoLabel->setText((QFileInfo::QFileInfo(Path).completeBaseName()));
    gridLayout_7->addWidget(videoLabel);
    connect(videoLabel,SIGNAL(VideoLabelClosed(QString)),(AllDisp*)(Viewer->widget(0)),SLOT(removeVideo(QString)));
}

void MainWindow::addCamLabelToPage2(int ind){
    if(labelNames.contains(QString::QString("Cam %1").arg(ind)))return;
    labelNames<<QString::QString("Cam %1").arg(ind);
    videoTitle* videoLabel=new videoTitle(ind,labelNames,this);
    connect(videoLabel,SIGNAL(exciteAlgoCheckBoxes(int)),this,SLOT(on_algo_1all_stateChanged(int)));
    connect(videoLabel,SIGNAL(exciteAlgoCheckBoxes(int)),this,SLOT(on_algo_2all_stateChanged(int)));
    connect(videoLabel,SIGNAL(exciteAlgoCheckBoxes(int)),this,SLOT(on_algo_3all_stateChanged(int)));
    on_algo_2all_stateChanged(4);
    on_algo_3all_stateChanged(4);
    on_algo_1all_stateChanged(4);
    videoLabel->setText(QString::QString("Cam %1").arg(ind));
    qDebug()<<QString::QString("Cam %1").arg(ind);
    gridLayout_3->addWidget(videoLabel);
    connect(videoLabel,SIGNAL(CameraLabelClosed(int)),(AllDisp*)(Viewer->widget(0)),SLOT(removeCam(int)));
    videoLabel->recordButton->setText(QString::QString("Cam %1").arg(ind));
    gridLayout_8->addWidget(videoLabel->recordButton);
}

void MainWindow::on_toolButton_clicked()
{
    bool ok;
    int i=QInputDialog::getInt(this,"Please enter the Camera Number you want Argus to Monitor","Camera Number",0,0,10,1,&ok,Qt::Tool);
    if(ok) emit sendCameraInfoToAll(i);
}

void MainWindow::on_algo_1sel_clicked()
{
    if(((AllDisp*)Viewer->widget(0))->myCamMap().size()==0) return;
    selectAlgo* Dialog=new selectAlgo(((AllDisp*)Viewer->widget(0))->myCamMap(),"algo 1",this);
    connect(Dialog,SIGNAL(algoCheckBoxStateUpdate(int)),this,SLOT(on_algo_1all_stateChanged(int)));
    Dialog->show();
}

void MainWindow::on_algo_2sel_clicked()
{
    if(((AllDisp*)Viewer->widget(0))->myCamMap().size()==0) return;
    selectAlgo* Dialog=new selectAlgo(((AllDisp*)Viewer->widget(0))->myCamMap(),"algo 2",this);
    connect(Dialog,SIGNAL(algoCheckBoxStateUpdate(int)),this,SLOT(on_algo_2all_stateChanged(int)));
    Dialog->show();
}

void MainWindow::on_algo_3sel_clicked()
{
    if(((AllDisp*)Viewer->widget(0))->myCamMap().size()==0) return;
    selectAlgo* Dialog=new selectAlgo(((AllDisp*)Viewer->widget(0))->myCamMap(),"algo 3",this);
    connect(Dialog,SIGNAL(algoCheckBoxStateUpdate(int)),this,SLOT(on_algo_3all_stateChanged(int)));
    Dialog->show();
}

void MainWindow::on_algo_3all_stateChanged(int n)
{
    if(((AllDisp*)Viewer->widget(0))->myCamMap().size()==0){
        algo_3all->setTristate(false);
        algo_3all->setCheckState(Qt::Unchecked);
        return;
    }
    if (n==4){
        selectAlgo* Dialog=new selectAlgo(((AllDisp*)Viewer->widget(0))->myCamMap(),"algo 3",this);
        n=Dialog->updateCheckBoxStatus();
        delete Dialog;
    }
    bool state;
    if (n==0) state=false;
    else if(n==2) state=true;
    else{
        algo_3all->setTristate(true);
        algo_3all->setCheckState(Qt::PartiallyChecked);
        return;
    }
    algo_3all->setCheckState((Qt::CheckState)n);
    algo_3all->setTristate(false);
    QList<QString>paths=((AllDisp*)Viewer->widget(0))->myCamMap().keys();
    foreach(QString path, paths){
        ((AllDisp*)Viewer->widget(0))->myCamMap()[path]->detectionMap["algo 3"]=state;
    }
}

void MainWindow::on_algo_2all_stateChanged(int n)
{
    if(((AllDisp*)Viewer->widget(0))->myCamMap().size()==0){
        algo_2all->setTristate(false);
        algo_2all->setCheckState(Qt::Unchecked);
        return;
    }
    if (n==4){
        selectAlgo* Dialog=new selectAlgo(((AllDisp*)Viewer->widget(0))->myCamMap(),"algo 2",this);
        n=Dialog->updateCheckBoxStatus();
        delete Dialog;
    }
    bool state;
    if (n==0) state=false;
    else if(n==2) state=true;
    else{
        algo_2all->setTristate(true);
        algo_2all->setCheckState(Qt::PartiallyChecked);
        return;
    }
    algo_2all->setCheckState((Qt::CheckState)n);
    algo_2all->setTristate(false);
    QList<QString>paths=((AllDisp*)Viewer->widget(0))->myCamMap().keys();
    foreach(QString path, paths){
        ((AllDisp*)Viewer->widget(0))->myCamMap()[path]->detectionMap["algo 2"]=state;
    }
}
void MainWindow::on_algo_1all_stateChanged(int n)
{
    if(((AllDisp*)Viewer->widget(0))->myCamMap().size()==0){
        algo_1all->setTristate(false);
        algo_1all->setCheckState(Qt::Unchecked);
        return;
    }
    if (n==4){
        selectAlgo* Dialog=new selectAlgo(((AllDisp*)Viewer->widget(0))->myCamMap(),"algo 1",this);
        n=Dialog->updateCheckBoxStatus();
        qDebug()<<n;
        delete Dialog;
    }
    bool state;
    if (n==0) state=false;
    else if(n==2) state=true;
    else{
        algo_1all->setTristate(true);
        algo_1all->setCheckState(Qt::PartiallyChecked);
        return;
    }
    algo_1all->setCheckState((Qt::CheckState)n);
    algo_1all->setTristate(false);
    QList<QString>paths=((AllDisp*)Viewer->widget(0))->myCamMap().keys();
    foreach(QString path, paths){
        ((AllDisp*)Viewer->widget(0))->myCamMap()[path]->detectionMap["algo 1"]=state;
    }
}
void MainWindow::updateThreatBar(QString path, int threat){
    threatLevel->setValue(threat);
   if(path=="All") threatLabel->setText("Threat Level at all feeds:");
   else threatLabel->setText(QString::QString("%1's Threat Level:").arg(QFileInfo::QFileInfo(path).baseName()));
}
