#include "detection.h"
#include"camdisp.h"
//activates the algorithm u want to run, all algorithm (should) be linkable and run on seperate threads
Detection::Detection(QString path, QWidget *parent):QThread(parent){
    recording=false;
    qDebug()<<"creating detection";
    theCameraImWorkingWith=NULL;
    theVideoImWorkingWith=path;
    HavingCapture=false;
    threatLevel=&((camDisp*)parent)->threat;
    *threatLevel=0;
}
Detection::Detection(int ind, QWidget *parent):QThread(parent){
    HavingCapture=false;
    theCameraImWorkingWith=ind;
    theVideoImWorkingWith="";
    recording=false;
    threatLevel=&((camDisp*)parent)->threat;
    *threatLevel=0;
}
void Detection::grabTypeOfDetection(QMap<QString, bool> detectionList){
    detectionsIHaveToMake=detectionList;
}
void Detection::IplImageToPixmapConverter(IplImage *frame){
    cvSplit(frame, tchannel1, tchannel2, tchannel3, NULL);
    cvMerge(tchannel1, tchannel2, tchannel3, tchannel0, dframe);
    uchar * data = (uchar *)(dframe->imageData);
    QPixmap QPframe=QPixmap::fromImage(QImage::QImage(data,dframe->width,dframe->height,dframe->widthStep,QImage::Format_RGB32));
    emit PixmapToBeDisplayed(QPframe);
//    qDebug()<<"emission";
}
void Detection::run(){
//    qDebug()<<"detection"<<theVideoImWorkingWith;
//    qDebug()<<"detection"<<theCameraImWorkingWith;
    if(theVideoImWorkingWith!=""){
        capture=cvCaptureFromFile(theVideoImWorkingWith.toLocal8Bit().data());
    }
    else{
        capture=cvCaptureFromCAM(theCameraImWorkingWith);
    }
    HavingCapture=true;
    int fps=(int) cvGetCaptureProperty(capture, CV_CAP_PROP_FPS);
    {//for wrapping
    SourceCapture=cvQueryFrame(capture);
    tchannel0=cvCreateImage(cvGetSize(SourceCapture), IPL_DEPTH_8U, 1);
    tchannel1=cvCreateImage(cvGetSize(SourceCapture), IPL_DEPTH_8U, 1);
    tchannel2=cvCreateImage(cvGetSize(SourceCapture),IPL_DEPTH_8U, 1);
    tchannel3= cvCreateImage(cvGetSize(SourceCapture),IPL_DEPTH_8U, 1);
    dframe=cvCreateImage(cvGetSize(SourceCapture),IPL_DEPTH_8U,4);
    grayImage=cvCreateImage(cvGetSize(SourceCapture),IPL_DEPTH_8U,1);
    smallImage=cvCreateImage(cvSize(SourceCapture->width/3,SourceCapture->height/3),IPL_DEPTH_8U,1);
    CASCADE_NAME1="C:/OpenCV2.2/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml";
    CASCADE_NAME2 ="C:/OpenCV2.2/data/haarcascades/haarcascade_frontalface_default.xml";
    CASCADE_NAME3 ="C:/OpenCV2.2/data/haarcascades/haarcascade_upperbody.xml";
//    qDebug()<<"reached";
    cascade2 = (CvHaarClassifierCascade*) cvLoad(CASCADE_NAME2, 0, 0, 0);
    cascade1 = (CvHaarClassifierCascade*) cvLoad(CASCADE_NAME1, 0, 0, 0);
    cascade3 = (CvHaarClassifierCascade*) cvLoad(CASCADE_NAME3, 0, 0, 0);
    storage = cvCreateMemStorage(0);
    temp=cvCloneImage((const IplImage*)SourceCapture);
    }
    forever{
        if(!SourceCapture){//for infinite playback
            cvReleaseCapture(&capture);
            HavingCapture=false;
            capture=cvCaptureFromFile(theVideoImWorkingWith.toLocal8Bit().data());
            HavingCapture=true;
            SourceCapture=cvQueryFrame(capture);
        }
        *temp = *SourceCapture;
        threatMath(SourceCapture);
//        qDebug()<<"done with first frame";
        *SourceCapture = *temp;
        IplImageToPixmapConverter(SourceCapture);
        SourceCapture=cvQueryFrame(capture);
        msleep(1000/fps);
    }
}
void Detection::threatMath(IplImage *copy){
    const int scale = 1.5;
    cvCvtColor(copy, grayImage,CV_BGR2GRAY);
    cvResize(grayImage,smallImage,CV_INTER_LINEAR);
    eyes = cvHaarDetectObjects (smallImage,
                                cascade1,
                                storage,
                                1.1,3,
                                CV_HAAR_DO_CANNY_PRUNING,
                                cvSize (20,20));
    faces = cvHaarDetectObjects(smallImage,
                                cascade2,
                                storage,
                                1.1,3,
                                CV_HAAR_SCALE_IMAGE,
                                cvSize (20,20));
    upperBody =cvHaarDetectObjects(smallImage,
                                   cascade3,
                                   storage,1.1,3,
                                   CV_HAAR_DO_CANNY_PRUNING,
                                   cvSize (35,45));
    int i = 0;
    for ( i ; i < (eyes ? eyes->total:0); i++)
    {
        CvRect* r1 = (CvRect*) cvGetSeqElem (eyes, i);
        CvPoint pt1, pt2;
        pt1.x = r1->x;
        pt2.x = (r1->x+r1->width);
        pt1.y = r1->y;
        pt2.y = (r1->y+r1->height);
        // Drawing rectangle
        qDebug()<<"inside eyedetect";
        cvRectangle(temp, pt1, pt2, CV_RGB(0,0,255), 2, 8, 0 );
    }
    int j = 0;
    for ( j ; j < (faces ? faces->total:0); j++)
    {
        CvRect* r2 = (CvRect*) cvGetSeqElem (faces, j);
        CvPoint pt3, pt4;
        pt3.x = r2->x*scale;
        pt4.x = (r2->x+r2->width)*scale;
        pt3.y = r2->y*scale;
        pt4.y = (r2->y+r2->height)*scale;
        // Drawing rectangle
        qDebug()<<"inside facedetect";
        cvRectangle(temp, pt3, pt4, CV_RGB(255,0,0), 2, 8, 0 );
    }
    int k = 0;
    for ( k ; k < (upperBody ? upperBody->total:0); k++)
    {
        CvRect* r3 = (CvRect*) cvGetSeqElem (upperBody, k);
        CvPoint pt5, pt6;
        pt5.x = r3->x;
        pt6.x = (r3->x+r3->width);
        pt5.y = r3->y;
        pt6.y = (r3->y+r3->height);
        qDebug()<<"inside UPPERBODY";
        // Drawing rectangle
        cvRectangle(temp, pt5, pt6, CV_RGB(0,255,0), 2, 8, 0 );
    }
    threatCalculate(eyes->total,faces->total,upperBody->total);
    qDebug()<<*threatLevel;
    cvClearMemStorage(storage);
}

void Detection::threatCalculate(int eye, int face, int ub){
    if(!eye and !face and !ub){
        *threatLevel=0;
        return;
    }
    *threatLevel=(int)(((0.10*eye)+(0.80*face)+(0.10*ub))/(eye+face+ub))*100;//is the equation that will be calculating threat level!
}

Detection::~Detection(){
    qDebug()<<"Destroying detection";
    if(HavingCapture)cvReleaseCapture(&capture);
    terminate();
}
