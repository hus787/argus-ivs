#-------------------------------------------------
#
# Project created by QtCreator 2011-07-09T19:13:43
#
#-------------------------------------------------

QT       += core gui

TARGET = designing
TEMPLATE = app

#LIBS += -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_flann

SOURCES += main.cpp\
        mainwindow.cpp \
    detection.cpp \
    camdisp.cpp \
    alldisp.cpp \
    viewer.cpp \
    videotitle.cpp \
    selectalgo.cpp

HEADERS  += mainwindow.h \
    all_includes.h \
    detection.h \
    camdisp.h \
    alldisp.h \
    viewer.h \
    videotitle.h \
    selectalgo.h

FORMS    += mainwindow.ui \
    selectalgo.ui

#----------------------------------------------
#
##My addition's start here
#
#----------------------------------------------


#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_core220
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_core220d

#INCLUDEPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include
#DEPENDPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_imgproc220
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_imgproc220d

#INCLUDEPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include
#DEPENDPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_ffmpeg220
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_ffmpeg220d

#INCLUDEPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include
#DEPENDPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_objdetect220
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_objdetect220d
#else:symbian: LIBS += -lopencv_objdetect220
#else:unix: LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_objdetect220

#INCLUDEPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include
#DEPENDPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_video220
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_video220d
#else:symbian: LIBS += -lopencv_video220
#else:unix: LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_video220

#INCLUDEPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include
#DEPENDPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_highgui220
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../../../../../OpenCV2.2/lib/ -lopencv_highgui220d

#INCLUDEPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include
#DEPENDPATH += $$PWD/../../../../../../../../../../OpenCV2.2/include

#############################################

unix:!macx:!symbian: LIBS += -L$$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/lib/ -lopencv_highgui

INCLUDEPATH += $$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/include
DEPENDPATH += $$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/include

unix:!macx:!symbian: LIBS += -L$$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/lib/ -lopencv_imgproc

INCLUDEPATH += $$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/include
DEPENDPATH += $$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/include

unix:!macx:!symbian: LIBS += -L$$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/lib/ -lopencv_core

INCLUDEPATH += $$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/include
DEPENDPATH += $$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/include

unix:!macx:!symbian: LIBS += -L$$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/lib/ -lopencv_objdetect

INCLUDEPATH += $$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/include
DEPENDPATH += $$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/include

unix:!macx:!symbian: LIBS += -L$$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/lib/ -lopencv_video

INCLUDEPATH += $$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/include
DEPENDPATH += $$PWD/../../../../../../../../../../../Desktop/OpenCV-2.3.1/include
