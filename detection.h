#ifndef DETECTION_H
#define DETECTION_H

#include"QThread"
#include"QtGui"
#include ""
//#include "camdisp.h"
class Detection:public QThread
{
    Q_OBJECT
public:
    Detection(QString path, QWidget* parent=0);
    Detection(int ind, QWidget*parent=0);
    ~Detection();
    void IplImageToPixmapConverter(IplImage* frame);
    QMap<QString,bool> detectionsIHaveToMake;
    bool recording;
    int* threatLevel;
signals:
    void informCamDispThreatUpdateed();
    void PixmapToBeDisplayed(QPixmap Frame);
public slots:
    void grabTypeOfDetection(QMap<QString,bool> detectionList);
protected:
    void run();
private:
    CvSeq* eyes;
    CvSeq* faces;
    CvSeq* upperBody;
    void threatMath(IplImage* copy);
    void threatCalculate(int eye, int face, int ub);
    const char* CASCADE_NAME1;
    const char* CASCADE_NAME2;
    const char* CASCADE_NAME3;
    IplImage* tchannel0;
    IplImage* tchannel1;
    IplImage* tchannel2;
    IplImage*  tchannel3;
    IplImage*  dframe;
    IplImage* SourceCapture;
    IplImage* grayImage;
    IplImage* smallImage;
    IplImage* temp;
    CvHaarClassifierCascade* cascade1;
    CvHaarClassifierCascade* cascade2;
    CvHaarClassifierCascade* cascade3;
    CvMemStorage* storage;
    bool HavingCapture;
    QString theVideoImWorkingWith;
    CvCapture* capture;
    int theCameraImWorkingWith;
};

#endif // DETECTION_H`
