/********************************************************************************
** Form generated from reading UI file 'selectalgo.ui'
**
** Created: Thu Oct 27 03:58:18 2011
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTALGO_H
#define UI_SELECTALGO_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_selectAlgo
{
public:
    QGridLayout *gridLayout;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *selectAlgo)
    {
        if (selectAlgo->objectName().isEmpty())
            selectAlgo->setObjectName(QString::fromUtf8("selectAlgo"));
        selectAlgo->resize(400, 300);
        gridLayout = new QGridLayout(selectAlgo);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        buttonBox = new QDialogButtonBox(selectAlgo);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 0, 0, 1, 1);


        retranslateUi(selectAlgo);
        QObject::connect(buttonBox, SIGNAL(accepted()), selectAlgo, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), selectAlgo, SLOT(reject()));

        QMetaObject::connectSlotsByName(selectAlgo);
    } // setupUi

    void retranslateUi(QDialog *selectAlgo)
    {
        selectAlgo->setWindowTitle(QApplication::translate("selectAlgo", "Dialog", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class selectAlgo: public Ui_selectAlgo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTALGO_H
